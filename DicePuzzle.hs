{-# LANGUAGE TupleSections #-}
{-# OPTIONS_GHC -Wall -Wno-missing-signatures -Wno-name-shadowing #-}
import Data.List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe

data Tile = Blank | Red | Green | Unknown
    deriving (Eq, Ord, Show)

data Vec = Vec {-# UNPACK #-} !Int {-# UNPACK #-} !Int {-# UNPACK #-} !Int
    deriving (Eq, Ord, Show)

type Direction = Vec
type Rotation = Vec

data Piece = Piece 
    { spec :: [(Vec, Direction, Tile)]
    , name :: String }
    deriving (Eq, Ord, Show)

data PlacedPiece = PlacedPiece
    { piece :: Piece
    , position :: Vec
    , rotation :: Rotation }
    deriving (Eq, Ord, Show)

instance Num Vec where
    fromInteger x = Vec (fromInteger x) (fromInteger x) (fromInteger x)
    Vec a b c + Vec x y z = Vec (a + x) (b + y) (c + z)
    Vec a b c * Vec x y z = Vec (a * x) (b * y) (c * z)
    negate (Vec a b c) = Vec (-a) (-b) (-c)
    abs (Vec a b c) = Vec (abs a) (abs b) (abs c)
    signum (Vec a b c) = Vec (signum a) (signum b) (signum c)

colorToSolve = Green

allRotations :: [Rotation]
allRotations = [Vec x 0 z | x <- [0..3], z <- [0..3]] ++ [Vec 0 y z | y <- [1, 3], z <- [0..3]]

cos90 :: Int -> Int
cos90 0 = 1
cos90 1 = 0
cos90 2 = -1
cos90 3 = 0

sin90 :: Int -> Int
sin90 0 = 0
sin90 1 = 1
sin90 2 = 0
sin90 3 = -1

rotateX :: Int -> Vec -> Vec
rotateX a (Vec x y z) = Vec x (cos90 a * y - sin90 a * z) (cos90 a * z + sin90 a * y)

rotateY :: Int -> Vec -> Vec
rotateY a (Vec x y z) = Vec (cos90 a * x - sin90 a * z) y (cos90 a * z + sin90 a * x)

rotateZ :: Int -> Vec -> Vec
rotateZ a (Vec x y z) = Vec (cos90 a * x - sin90 a * y) (cos90 a * y + sin90 a * x) z

rotateVec :: Rotation -> Vec -> Vec
rotateVec (Vec rotX rotY rotZ) =
    rotateZ (rotZ `mod` 4) . rotateY (rotY `mod` 4) . rotateX (rotX `mod` 4)

piecePositions = [Vec 0 0 0, Vec 1 0 0, Vec 0 1 0]

fillForPiece :: PlacedPiece -> [Vec]
fillForPiece (PlacedPiece _ position rotation) =
    map ((+ position) . rotateVec rotation) piecePositions

notOutside :: Vec -> Bool
notOutside (Vec x y z) = all (\a -> a >= 0 && a <= 2) [x, y, z]

noDuplicates :: Ord a => [a] -> Bool
noDuplicates = not . hasDuplicatesSorted . sort
    where
    hasDuplicatesSorted (x : y : xs) = x == y || hasDuplicatesSorted (y : xs)
    hasDuplicatesSorted _ = False

validFill :: Set PlacedPiece -> Bool
validFill pieces = noDuplicates positions && all notOutside positions
    where
    positions = concatMap fillForPiece (Set.toList pieces)

tilesForPiece :: PlacedPiece -> Map (Vec, Direction) Tile
tilesForPiece (PlacedPiece piece position rotation) = Map.fromList $ map transform (spec piece)
    where
    transform (pos, dir, tile) = ((rotateVec rotation pos + position, rotateVec rotation dir), tile)

withRotatedVariant :: [String] -> [[String]]
withRotatedVariant lines = [lines, transpose lines]

correctSides =
    [
        [
            [ "   "
            , " # "
            , "   " ]
        ]
    ,
        withRotatedVariant
            [ "#  "
            , "   "
            , "  #" ]
    ,
        withRotatedVariant
            [ "#  "
            , " # "
            , "  #" ]
    ,
        [
            [ "# #"
            , "   "
            , "# #" ]
        ]
    ,
        [
            [ "# #"
            , " # "
            , "# #" ]
        ]
    ,
        withRotatedVariant
            [ "###"
            , "   "
            , "###" ]
    ]

makeMask :: [String] -> [Tile]
makeMask = concatMap (fmap (\c -> if c == '#' then colorToSolve else Blank))

sideMasks :: [([[Tile]], Int)]
sideMasks = zip (fmap (fmap makeMask) correctSides) [1..6]

invDir :: Direction -> Vec
invDir dir = 1 - abs dir

diceSidePositions :: Direction -> [Vec]
diceSidePositions dir = Set.toList $ Set.fromList
    [invDir dir * Vec x y z + dir + 1 | x <- [-1..1], y <- [-1..1], z <- [-1..1]]

diceSideTiles :: Direction -> Map (Vec, Direction) Tile -> [Tile]
diceSideTiles dir mp =
    map (fromMaybe Unknown . (`Map.lookup` mp) . (, dir)) positions
    where
    positions = diceSidePositions dir

matchesFace :: [Tile] -> [Tile] -> Bool
matchesFace tiles face = and $ zipWith (\t1 t2 -> t1 == t2 || t1 == Unknown || t2 == Unknown) tiles face

identifySide :: [Tile] -> [Int]
identifySide face = map snd $ filter (\(variants, _) -> any (matchesFace face) variants) sideMasks

pieceFromStrips :: ([Tile], [Tile], [Tile]) -> [(Vec, Direction, Tile)]
pieceFromStrips (front, back, side) =
    zipWith3 (\t v d -> (v, d, t))
        side
        [Vec 0 1 0, Vec 0 1 0, Vec 0 0 0, Vec 0 0 0, Vec 1 0 0, Vec 1 0 0]
        [Vec 0 1 0, Vec (-1) 0 0, Vec (-1) 0 0, Vec 0 (-1) 0, Vec 0 (-1) 0, Vec 1 0 0]
    <> zipWith (\t v -> (v, Vec 0 0 1, t)) back [Vec 0 1 0, Vec 0 0 0, Vec 1 0 0]
    <> zipWith (\t v -> (v, Vec 0 0 (-1), t)) front [Vec 0 1 0, Vec 0 0 0, Vec 1 0 0]

allDirections = [[Vec x 0 0 | x <- [-1, 1]], [Vec 0 x 0 | x <- [-1, 1]], [Vec 0 0 x | x <- [-1, 1]]]

validFaces :: Set PlacedPiece -> Bool
validFaces pieces = all (not . null) (concat allFaces) && all opposite7 allFaces
    where
    allTiles = foldMap tilesForPiece pieces
    allFaces = fmap (fmap (\dir -> identifySide (diceSideTiles dir allTiles))) allDirections
    opposite7 [[f1], [f2]] = f1 + f2 == 7
    opposite7 _ = True

charToTile 'r' = Red
charToTile ' ' = Blank
charToTile 'g' = Green
charToTile _ = error "unknown tile char"

decodePiece str = Piece (pieceFromStrips (f, b, s)) str
    where
    (front, str') = splitAt 3 str
    (back, side) = splitAt 3 str'
    [f, b, s] = fmap (fmap charToTile) [front, back, side]

pieces = fmap decodePiece
    [ "gg  rrgg  rr"
    , " g r rr    r"
    , "r   g r  g r"
    , " r g  g r  g"
    , "  gr rrr  gg"
    , "g gr   r  gg"
    , "  r  ggg r  "
    , "rr    g r   "
    , "   gg      r" ]

data SolveState = SolveState
    { placed :: Set PlacedPiece
    , remaining :: [Piece] }
    deriving (Show)

allPlacements = [(Vec x y z, rot) | x <- [0..2], y <- [0..2], z <- [0..2], rot <- allRotations]

nextStates :: SolveState -> [SolveState]
nextStates (SolveState placed remaining) =
    [ SolveState newPlaced rest
    | let (selected : rest) = remaining
    , (pos, rot) <- allPlacements
    , let pp = PlacedPiece selected pos rot
    , let newPlaced = Set.insert pp placed
    , validFill newPlaced
    , validFaces newPlaced ]

winState :: SolveState -> Bool
winState (SolveState _ []) = True
winState _ = False

solve :: Int -> SolveState -> (Int, Maybe SolveState)
solve count st
    | winState st = (count, Just st)
    | otherwise = go count (nextStates st)
    where
    go c (st : sts) = case ms of
        Nothing -> go c' sts
        Just sol -> (c', Just sol)
        where
        (c', ms) = solve c st
    go c [] = (c, Nothing)

main = do
    let (count, Just (SolveState solution _)) = solve 0 (SolveState Set.empty pieces)
    print count
    print (fmap (\(PlacedPiece (Piece _ name) pos rot) -> (name, pos, rot)) (Set.toList solution))