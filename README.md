# The puzzle

The puzzle consists of 9 L-shaped wooden pieces that, when put together correctly
form a cube. The pieces have red and green dots on them. The goal is to assemble
a cube in such a way that the dots on the pieces make the cube into a standard
die. 

An example can be seen on Amazon https://www.amazon.com/Dice-Wooden-Brain-Teaser-Puzzle/dp/B005K2WF1A

# The solver

The problem is modeled using a few types. A 3 dimensional vector with `Int` coordinates is used to describe positions, directions and rotations. A tile can be Red, Green, Blank or Unknown. Pieces are described as a list of (position, direction, tile) tuples. It's assumed that pieces are originally oriented so they look like the letter L.

Then for a given configuration of placed pieces we collect a list of positions they fill and check it for duplicated. Duplicates mean that two pieces overlap so the configuration is not a valid solution. We also check if any of the pieces are out of bounds. 

Correct sides are described by lists of strings where blanks and dots are encoded with different characters. For a given configuration of pieces we collect all the tiles. Each cube face is checked by getting all the tiles that are on it and cross-referencing the list of valid faces to see which number is displayed on it. If we don't find a valid face, the configuration is discarded. If opposite faces show numbers that don't add up to 7, the configuration is discarded.

The actual pieces in the real life puzzle are described using a string of characters, each char representing one of the tiles on the piece.

Then the solver goes through each piece and tries to place it in each position using all the possible rotations. The number of "moves" is pretty big (~700) but a great majority of them are invalid so they are immediately rejected. Once a valid configuration with all the pieces placed is found, the solver returns it and terminates.

# Building

The code can be built using GHC 8.6 or later (and probably with earlier versions as well). It depends only on packages included in the Haskell Platform.
